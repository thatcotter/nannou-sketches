use draw_helpers::line_circle;
use nannou::prelude::*;

struct Model {}

fn main() {
    nannou::app(model).update(update).simple_window(view).run();
}

fn model(_app: &App) -> Model {
    Model {}
}

fn update(_app: &App, _model: &mut Model, _update: Update) {}

fn view(app: &App, _model: &Model, frame: Frame) {
    let draw = app.draw();
    let t = app.time;
    let _win = app.window_rect();

    draw.background().color(Rgb::new(0.02, 0.02, 0.02));

    line_circle(
        &app,
        &draw,
        pt2(0.0, 0.0),
        64,
        240.0,
        (t * 0.5).sin() * PI * 0.125,
    );

    draw.to_frame(app, &frame).unwrap();
}

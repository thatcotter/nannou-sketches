use nannou::prelude::*;

struct Particle {
    position: Point2,
    velocity: Vector2,
    acceleration: Vector2,
    life_span: f32,
}

impl Particle {
    fn new(l: Point2) -> Self {
        let acceleration = vec2(0.0, 0.0);
        let velocity = vec2(random_f32() * 2.0 - 1.0, random_f32() - 1.0);
        let position = l;
        let life_span = 255.0;
        Particle {
            position,
            velocity,
            acceleration,
            life_span,
        }
    }

    fn update(&mut self) {
        self.velocity += self.acceleration;
        self.position -= self.velocity;
        // self.life_span -= 2.0;
    }

    fn display(&self, draw: &Draw) {
        let size = 5.0 + (255.0 - self.life_span) * 0.13;
        draw.ellipse()
            .xy(self.position)
            .w_h(size, size)
            .color(WHITE);
    }
}

struct Model {
    particles: Vec<Particle>,
}

fn main() {
    nannou::app(model).update(update).simple_window(view).run();
}

fn model(app: &App) -> Model {
    let mut particles: Vec<Particle> = Vec::new();

    let win = app.window_rect();

    for _i in 0..50 {
        let p = Particle::new(pt2(
            (random_f32() - 0.5) * win.w(),
            (random_f32() - 0.5) * win.h(),
        ));

        particles.push(p);
    }

    Model { particles }
}

fn update(app: &App, model: &mut Model, _update: Update) {
    for i in 0..model.particles.len() {
        if model.particles[i].position.x < app.window_rect().left()
            || model.particles[i].position.x > app.window_rect().right()
        {
            model.particles[i].velocity.x *= -1.0;
        }
        if model.particles[i].position.y < app.window_rect().bottom()
            || model.particles[i].position.y > app.window_rect().top()
        {
            model.particles[i].velocity.y *= -1.0;
        }
        model.particles[i].update();
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();
    let _t = app.time;
    let _win = app.window_rect();

    draw.background().rgb(0.02, 0.02, 0.02);

    for p in &model.particles {
        p.display(&draw);
    }

    let mut radius = 120;

    while radius > 5 {
        for i in 0..model.particles.len() {
            draw.ellipse()
                .xy(model.particles[i].position)
                .radius(radius as f32)
                .color(BLACK);
        }
        radius -= 6;

        for i in 0..model.particles.len() {
            draw.ellipse()
                .xy(model.particles[i].position)
                .radius(radius as f32)
                .color(WHITE);
        }
        radius -= 6;
    }

    draw.to_frame(app, &frame).unwrap();
}

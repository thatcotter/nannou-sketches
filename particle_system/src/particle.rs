use nannou::noise::{NoiseFn, Perlin};
use nannou::prelude::*;
use nannou::rand::random_range;

#[derive(Debug, Copy, Clone)]
pub struct Particle {
    position: Point2,
    speed: Point2,
    acceleration: Point2,
    force: Point2,
    mass: f32,
    friction: f32,
    active: bool,
    age: f32,
    lifetime: f32,
    size: Point2,
    tail: f32,
    noise_seed: f32,
}

impl Particle {
    pub fn new() -> Particle {
        Particle {
            position: pt2(0.0, 0.0),
            speed: pt2(0.0, 0.0),
            acceleration: pt2(0.0, 0.0),
            force: pt2(0.0, 0.0),
            mass: 1.0,
            friction: 0.12,
            active: false,
            age: 0.0,
            lifetime: 200.0,
            size: pt2(5.0, 5.0),
            tail: 0.0,
            noise_seed: 0.0,
        }
    }

    pub fn set_position(&mut self, position: Point2) {
        self.position = position
    }

    pub fn randomize_speed(&mut self) {
        self.speed = pt2(random_range(-1.0, 1.0), random_range(-1.0, 1.0));
    }

    pub fn randomized_speed(mut self) -> Particle {
        self.speed = pt2(random_range(-1.0, 1.0), random_range(-1.0, 1.0));
        self
    }

    pub fn randomize_lifetime(&mut self) {
        self.lifetime = random_range(50.0, 100.00);
    }

    pub fn is_active(&self) -> bool {
        self.active
    }

    pub fn set_seed(&mut self, new_seed: f32) {
        self.noise_seed = new_seed;
    }

    pub fn reset_seed(&mut self) {
        self.noise_seed = random_range(0.0, 5.0);
    }

    pub fn set_active(&mut self) {
        self.active = true;
        self.age = 0.0;
        self.randomize_speed();
        self.randomize_lifetime();
    }

    pub fn add_noise_acceleration(&mut self) {
        let noise = Perlin::new();
        self.acceleration = pt2(
            noise.get([
                self.position.x as f64 * 0.01,
                self.position.y as f64 * 0.01,
                self.age as f64 * 0.01,
                self.noise_seed as f64,
            ]) as f32,
            noise.get([
                (self.position.y + 10.0) as f64 * 0.01,
                (self.position.x + 10.0) as f64 * 0.01,
                self.age as f64 * 0.01,
                self.noise_seed as f64,
            ]) as f32,
        );
    }

    pub fn update(&mut self) {
        self.age += 1.0;
        self.active = self.age < self.lifetime;
        // self.acceleration += self.force;
        self.add_noise_acceleration();
        self.acceleration *= self.friction;
        self.speed += self.acceleration;
        self.position += self.speed;
    }

    pub fn draw(&self, draw: &app::Draw) {
        let age = self.age / self.lifetime;
        let life_left = 1.0 - age;

        draw.line()
            .start(self.position)
            .end(self.position + self.speed * (10.0 * life_left + 5.0))
            .weight(3.0)
            .color(rgba(0.9 - 0.3 * age, 0.6 * life_left, 0.6 * age, life_left));
    }

    // Based on some old cinder code that I wrote a while back
    // ci::gl::begin(GL_LINES);
    // for (auto& p: mParticles){
    //     ci::gl::color(ci::ColorA(p->getTimerleft()/p->getLifespan() ,0.3f, 1.f - p->getTimerleft()/p->getLifespan(), p->getTimerleft()/p->getLifespan()));
    //     // get perlin value based on position
    //     glm::vec3 norm = mPerlin.dfBm( glm::vec3( p->getPosition().x, p->getPosition().y, mTime ) * mFrequency ) * 20.0f;
    //     glm::vec2 flowForce = normalize( glm::vec2( norm.x, norm.y ) ) * 0.1f;
    //     p->applyForce(flowForce);
    //     ci::gl::vertex(p->getPosition());
    //     ci::gl::vertex(p->getPosition() + glm::vec2(norm.x, norm.y) * 0.3f);
    // }
    // ci::gl::end();
}

use nannou::prelude::*;

mod particle;
use particle::Particle;

type ParticleSystem = Vec<Particle>;

struct Model {
    max_particles: i32,
    particles: ParticleSystem,
    mouse_pos: Point2,
    mouse_is_pressed: bool,
}

impl Model {
    fn reactivate_particles(&mut self, n: i32, pos: Point2, time: f32) {
        let mut count = 0;

        for i in 0..self.particles.len() {
            if n <= count {
                break;
            } else if !self.particles[i].is_active() {
                count += 1;
                self.particles[i].set_active();
                self.particles[i].set_position(pos);
                self.particles[i].set_seed(time);
            }
        }
    }
    fn get_particle_system(&self) -> ParticleSystem {
        self.particles.clone()
    }
    fn active_particles(&self) -> ParticleSystem {
        self.get_particle_system()
            .into_iter()
            .filter(|x| !(*x).is_active())
            .collect::<ParticleSystem>()
    }
}

fn main() {
    nannou::app(model).update(update).view(view).run();
}

fn model(app: &App) -> Model {
    app.new_window()
        .size(800, 600)
        .mouse_pressed(mouse_pressed)
        .mouse_moved(mouse_moved)
        .mouse_released(mouse_released)
        .build()
        .unwrap();
    let max_particles = 1000;
    let particles = (0..max_particles)
        .into_iter()
        .map(|_| Particle::new().randomized_speed())
        .collect();
    Model {
        max_particles: max_particles,
        particles: particles,
        mouse_pos: pt2(0.0, 0.0),
        mouse_is_pressed: false,
    }
}

fn update(app: &App, model: &mut Model, update: Update) {
    if model.mouse_is_pressed {
        model.reactivate_particles(3, model.mouse_pos, app.time);
    }

    for i in 0..model.particles.len() {
        if model.particles[i].is_active() {
            model.particles[i].update();
        }
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();

    frame.clear(rgb(0.0, 0.0, 0.0));

    for particle in &model.particles {
        if particle.is_active() {
            particle.draw(&draw);
        }
    }

    draw.to_frame(app, &frame).unwrap();
}

fn mouse_pressed(app: &App, model: &mut Model, button: MouseButton) {
    model.mouse_is_pressed = true;
}

fn mouse_released(app: &App, model: &mut Model, button: MouseButton) {
    model.mouse_is_pressed = false;
}

fn mouse_moved(app: &App, model: &mut Model, pos: Point2) {
    model.mouse_pos = pos;
}

use nannou::noise::{NoiseFn, Perlin};
use nannou::prelude::*;

struct Model {}

fn main() {
    nannou::app(model).update(update).simple_window(view).run();
}

fn model(app: &App) -> Model {
    Model {}
}

fn update(app: &App, model: &mut Model, update: Update) {}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();
    let t = app.time;
    let win = app.window_rect();
    let noise = Perlin::new();

    draw.background().color(Rgb::new(0.75, 0.75, 0.75));

    let n_points = 256;
    let half_thickness = 1.0;

    for y_off in (-300..300).step_by(36) {
        let vertices = (0..n_points)
            .map(|i| {
                let x = map_range(i, 0, n_points - 1, win.left() + 200.0, win.right() - 200.0);
                let n = noise.get([(x * 0.1) as f64, y_off as f64, (t * 0.75) as f64]) as f32;
                let y = n * 20.0 + y_off as f32;
                pt2(x, y)
            })
            .enumerate()
            .map(|(i, p)| {
                let r = ((p.angle() + t).sin() + 1.0) * 0.4;
                let g = ((p.angle() + t + TAU * 0.33).sin() + 1.0) * 0.4;
                let b = ((p.angle() + t + TAU * 0.66).sin() + 1.0) * 0.4;
                let rgba = nannou::color::Rgba::new(r, g, b, 1.0);
                geom::vertex::Srgba(p, rgba)
            });

        draw.polyline().vertices(half_thickness, vertices);
    }

    draw.to_frame(app, &frame).unwrap();
}

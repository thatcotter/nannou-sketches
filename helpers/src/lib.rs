use nannou::prelude::*;

pub fn lerp_colors(a: Rgba, b: Rgba, pct: f32) -> Rgba {
    let r = a.red + (b.red - a.red) * pct;
    let g = a.green + (b.green - a.green) * pct;
    let b = a.blue + (b.blue - a.blue) * pct;
    let a = 1.0;
    Rgba::new(r, g, b, a)
}

pub fn xy_from_theta(theta: f32) -> Point2 {
    let x = theta.cos();
    let y = theta.sin();
    pt2(x, y)
}

pub fn map_f32(value: f32, min1: f32, max1: f32, min2: f32, max2: f32) -> f32 {
    min2 + (value - min1) * (max2 - min2) / (max1 - min1)
}

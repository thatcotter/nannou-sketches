extern crate nannou;
use nannou::prelude::*;

fn main() {
    nannou::app(model)
        .update(update)
        .simple_window(view)
        .run();
}

struct Model {
	points: [Point2; 10000]
}

fn model(_app: &App) -> Model {
	let mut points: [Point2; 10000] 
		= [Vector2::new(0.0,0.0); 10000];
	for i in 0..10000 {
		// add points
		points[i] = plot_point(i as i32);
	}
    Model {
		points
	}
}

fn update(_app: &App, _model: &mut Model, _update: Update) {
}

fn view(app: &App, model: &Model, frame: Frame){
	let draw = app.draw();
    frame.clear(WHITE);

	for point in model.points {
		draw.ellipse()
			.color(BLACK)
			.xy(point)
			.w_h(3.0, 3.0);
	}

	draw.to_frame(app, &frame).unwrap();
}


fn plot_point(index: i32) -> Point2 {
	let magnitude = index as f32 / 10000.0;
	let x = (magnitude*280.0).sin() * magnitude * 420.0;
	let y = (magnitude*420.0).cos() * magnitude * 280.0;
	Vector2::new(x,y)
}
use nannou::prelude::*;

struct Model {}

fn main() {
    nannou::app(model).update(update).simple_window(view).run();
}

fn model(_app: &App) -> Model {
    Model {}
}

fn update(_app: &App, _model: &mut Model, _update: Update) {}

fn view(app: &App, _model: &Model, frame: Frame) {
    let draw = app.draw();
    let t = app.time;
    let _win = app.window_rect();

    draw.background().rgb(0.02, 0.02, 0.02);

    let num_points = 36;
    let outer_radius = 300.0;
    let inner_radius = 60.0;
    let mut size = 100;
    let slow_time = t * 0.05125;

    while size > 5 {
        let mut x2: f32;
        let mut y2: f32;

        for i in 0..num_points {
            let x1 = (slow_time + i as f32 * TAU / num_points as f32).cos() * outer_radius;
            let y1 = (slow_time + i as f32 * TAU / num_points as f32).sin() * outer_radius;

            x2 = x1 + (slow_time + x1 * TAU / num_points as f32).cos() * inner_radius;
            y2 = y1 + (slow_time + y1 * TAU / num_points as f32).sin() * inner_radius;

            draw.ellipse().x_y(x2, y2).radius(size as f32).color(WHITE);
        }
        size -= 6;

        for i in 0..num_points {
            let x1 = (slow_time + i as f32 * TAU / num_points as f32).cos() * outer_radius;
            let y1 = (slow_time + i as f32 * TAU / num_points as f32).sin() * outer_radius;

            x2 = x1 + (slow_time + x1 * TAU / num_points as f32).cos() * inner_radius;
            y2 = y1 + (slow_time + y1 * TAU / num_points as f32).sin() * inner_radius;
            draw.ellipse().x_y(x2, y2).radius(size as f32).color(BLACK);
        }
        size -= 6;
    }

    draw.to_frame(app, &frame).unwrap();
}

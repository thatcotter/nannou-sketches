use nannou::prelude::*;

struct Model {}

fn main() {
    nannou::app(model).update(update).simple_window(view).run();
}

fn model(_app: &App) -> Model {
    Model {}
}

fn update(_app: &App, _model: &mut Model, _update: Update) {}

fn view(app: &App, _model: &Model, frame: Frame) {
    let draw = app.draw();
    let t = app.time;
    let win = app.window_rect();

    draw.background().color(Rgb::new(0.75, 0.75, 0.75));

    for i in win.left() as i32..win.right() as i32 {
        let x = i as f32;
        draw.ellipse()
            .x_y(
                x,
                (x * 0.05 + t).sin() * 75.0 + (x * 0.0125 + t).sin() * 50.0,
            )
            .radius((x * 0.025 + t).sin().abs() * 25.0 + (x * 0.0125 + t).cos().abs() * 50.0)
            .rgb(
                (x * 0.00155 + t).sin().abs(),
                (x * 0.00225 + t).cos().abs(),
                (x * 0.00345 + t).sin().abs(),
            );
    }

    draw.to_frame(app, &frame).unwrap();
}

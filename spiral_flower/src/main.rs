use nannou::noise::{NoiseFn, Perlin};
use nannou::prelude::*;
use nannou::ui::prelude::*;

struct Model {
    debug: bool,
    ui: Ui,
    ids: Ids,
    num_petals: i32,
    min_distance: f32,
    step_size: f32,
    size_fluctuation: f32,
}

struct Ids {
    num_petals: widget::Id,
    min_distance: widget::Id,
    step_size: widget::Id,
    size_fluctuation: widget::Id,
}

fn main() {
    nannou::app(model).update(update).run();
}

fn model(app: &App) -> Model {
    app.new_window()
        .key_pressed(key_pressed)
        .view(view)
        .build()
        .unwrap();

    let mut ui = app.new_ui().build().unwrap();

    let ids = Ids {
        num_petals: ui.generate_widget_id(),
        min_distance: ui.generate_widget_id(),
        step_size: ui.generate_widget_id(),
        size_fluctuation: ui.generate_widget_id(),
    };

    let debug = false;
    let num_petals = 240;
    let min_distance = 80.0;
    let step_size = 1.0;
    let size_fluctuation = 4.0;

    Model {
        debug,
        ui,
        ids,
        num_petals,
        min_distance,
        step_size,
        size_fluctuation,
    }
}

fn key_pressed(_app: &App, model: &mut Model, key: Key) {
    match key {
        Key::Tab => {
            model.debug = !model.debug;
        }
        _ => {}
    }
}

fn update(_app: &App, model: &mut Model, _update: Update) {
    let ui = &mut model.ui.set_widgets();

    fn slider(val: f32, min: f32, max: f32) -> widget::Slider<'static, f32> {
        widget::Slider::new(val, min, max)
            .w_h(200.0, 30.0)
            .label_font_size(15)
            .rgb(0.3, 0.3, 0.3)
            .label_rgb(1.0, 1.0, 1.0)
            .border(0.0)
    }

    for value in slider(model.num_petals as f32, 10.0, 360.0)
        .top_left_with_margin(20.0)
        .label("Petals")
        .set(model.ids.num_petals, ui)
    {
        model.num_petals = value as i32;
    }

    for value in slider(model.min_distance as f32, 1.0, 360.0)
        .down(10.0)
        .label("Minimum Distance")
        .set(model.ids.min_distance, ui)
    {
        model.min_distance = value as f32;
    }

    for value in slider(model.step_size as f32, 0.0, 20.0)
        .down(10.0)
        .label("Step")
        .set(model.ids.step_size, ui)
    {
        model.step_size = value as f32;
    }

    for value in slider(model.size_fluctuation as f32, 0.0, 20.0)
        .down(10.0)
        .label("Size Fluctuation")
        .set(model.ids.size_fluctuation, ui)
    {
        model.size_fluctuation = value as f32;
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();
    let t = app.time;
    let win = app.window_rect();
    let PHI = 1.618033988749895;

    draw.background().color(Rgb::new(0.01, 0.01, 0.05));

    for i in 0..model.num_petals {
        let theta = t * 0.25 + i as f32;
        let distance = model.min_distance + theta * model.step_size;

        let r = theta.sin();
        let g = (theta + TAU * 0.33).sin();
        let b = (theta + TAU * 0.66).sin();

        draw.ellipse()
            .radius(10.0 + (t + i as f32 + distance * 0.25).sin() * model.size_fluctuation)
            .rgb(r, g, b)
            .x_y(theta.cos() * distance, theta.sin() * distance);
    }

    draw.to_frame(app, &frame).unwrap();
    if model.debug {
        model.ui.draw_to_frame(app, &frame).unwrap();
    }
}

use nannou::prelude::*;

#[derive(Debug)]
struct Square {
    a: f32,
    b: f32,
}

struct Model {
    // grid: Vec<Vec<Square>>,
// next: Vec<Vec<Square>>,
}

fn main() {
    nannou::app(model).update(update).run();
}

fn model(app: &App) -> Model {
    let _window_size = pt2(640, 480);

    app.new_window()
        //    .with_dimensions(window_size, window_size)
        .event(event)
        .view(view)
        .build()
        .unwrap();

    Model {}
}

fn update(_app: &App, _model: &mut Model, _update: Update) {}

fn event(_app: &App, _model: &mut Model, event: WindowEvent) {
    // println!("{:?}", event);
    match event {
        MouseMoved(_pos) => {}
        MousePressed(_button) => {}
        MouseReleased(_button) => {}
        _ => {}
    }
}

fn view(app: &App, _model: &Model, frame: Frame) {
    let draw = app.draw();
    let _t = app.time;
    let _win = app.window_rect();

    draw.background().rgb(0.15, 0.15, 0.15);

    draw.to_frame(app, &frame).unwrap();
}

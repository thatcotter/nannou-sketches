extern crate nannou;
use nannou::draw::mesh::vertex::Color;
use nannou::draw::properties::{SetColor, SetDimensions};
use nannou::{prelude::*};
use nannou::noise::{NoiseFn, Perlin, Seedable};
use nannou::rand::{random_range};

struct Model {
	noise: Perlin,
	noise_scale: f32,
	noise_strength: f32,
	stroke: Stroke,
	palette: Vec<Rgb>,
	min_width: f32,
	max_width: f32
}

fn main() {
    nannou::app(model).update(update).run();
}

fn model(app: &App) -> Model {
	app.new_window()
    	.view(view)
		.key_pressed(key_pressed)
        .build()
        .unwrap();
	// app.set_loop_mode(LoopMode::loop_once());
    let window = app.window_rect();

	let min_width = 2.0;
	let max_width = 4.0;
	let palette = vec![
		rgb(21.0/ 255.0, 96.0/ 255.0, 100.0/ 255.0),
		rgb(0.0 / 255.0, 196.0 / 255.0, 154.0 / 255.0),
		rgb(248.0 / 255.0, 225.0 / 255.0, 108.0 / 255.0),
		rgb(255.0 / 255.0, 194.0 / 255.0, 180.0 / 255.0),
        rgb(251.0 / 255.0, 143.0 / 255.0, 103.0 / 255.0),
	];

	let noise = Perlin::new().set_seed(random_range(0, 10000));
	let noise_scale = 0.0125;
	let noise_strength = 360.0;
	let mut stroke = Stroke::new(0, Vec2::new(0.0, 0.0), palette.clone(), noise, min_width, max_width);
    
	for i in 0..10000 {
        let i = i as f32;
        let point = Vec2::new(
			(i/180.0*PI * window.top()/1000.0).cos() * i * window.right() / 10000.0,
			(i/180.0*PI * window.right()/1000.0).sin() * i * window.right() / 10000.0
		);

		stroke.path.push(point);
	};

    Model {
		noise,
		noise_scale,
		noise_strength,
		stroke,
        palette,
		min_width,
		max_width
	}
}

fn key_pressed(app: &App, model: &mut Model, key: Key) {
	// clear_strokes(model.strokes);
	match key {
		Key::Space => {
			// model.strokes.into_iter().for_each(|mut stroke| {
			// 	stroke.path = Vec::new();
			// });
			let win = app.window_rect();
			// for stroke in &mut model.stroke {
			// 	stroke.path = vec![Vec2::new(
			// 		random_range(win.left(), win.right()),
			// 		random_range(win.top(), win.bottom())
			// 	)];
			// 	stroke.active = true;
			// 	model.noise = Perlin::new().set_seed(random_range(0, 10000));
			// }
		}
		Key::Tab => {
			// toggle ui
			// model.show_ui = !model.show_ui;
		}
		_ => {}
	}
}

fn update(app: &App, model: &mut Model, _update: Update) {
	let window = app.window_rect();
	let t = app.time * 0.1;
	for i in 0..model.stroke.path.len() {
        let index = i as f32;
		let prev_point = model.stroke.path[i];
		let nx = model.noise.get([
			(prev_point.x * model.noise_scale) as f64, 
			(prev_point.y * model.noise_scale) as f64, 
			(index * model.noise_scale) as f64
		]) as f32 * model.noise_strength;
		let ny = model.noise.get([
			(prev_point.y * model.noise_scale) as f64, 
			(prev_point.x * model.noise_scale) as f64, 
			(index * model.noise_scale) as f64
		]) as f32 * model.noise_strength;

        let point = Vec2::new(
			((index+t)/180.0 * PI).cos() * (index + nx) * window.right() / 10000.0,
			((index+t)/180.0 * PI).sin() * (index + ny) * window.right() / 10000.0
		);

		model.stroke.path[i] = point;
	};

}

fn view(app: &App, model: &Model, frame: Frame){
    // frame.clear(WHITE);
	// frame.clear(rgb(0.05, 0.05, 0.05));
    // frame.clear(rgb(0.95, 0.95, 0.95));
	frame.clear(rgb(251.0 / 320.0, 143.0 / 320.0, 103.0 / 320.0));
    
	let draw = app.draw();
	// let win = app.window_rect();
	let t = app.time * 0.1;
    model.stroke.draw(&draw, model.noise, model.noise_scale, model.noise_strength, t);

    draw.to_frame(app, &frame).unwrap();
}

#[derive(Clone)]
struct Stroke {
	path: Vec<Point2>,
	width: f32,
	color: Vec<Rgb>,
    noise: Perlin,
	active: bool,
	decay_chance: f32,
	id: u16
}

impl Stroke {

    fn new(id: u16, start: Vec2, palette: Vec<Rgb>, noise: Perlin, min_width: f32, max_width: f32) -> Stroke {
        Stroke {
            path: vec![start],
            width: random_range(min_width, max_width),
            color: palette,
            noise,
            active: true,
            decay_chance: -1.0,
            id
        }
    }

	fn update(&mut self, noise: Perlin, noise_scale: f32, noise_strength: f32, others: &Vec<Stroke>) {
		let pos = self.path[self.path.len()-1];

		for other in others {
			if self.id == other.id {
				continue;
			}
			for other_pos in &other.path{
				// if pos == *other_pos { continue; } 
				if pos.distance(*other_pos) < self.width * 0.66 {
					self.active = false;
					return;
				}
			}
		}

		if self.path.len() > 5 && random_f32() < self.decay_chance {
			self.active = false;
			return;
		}

		self.path.push(pos + Point2::new(
			noise.get([(pos.x * noise_scale) as f64, (pos.y * noise_scale) as f64]) as f32 * noise_strength,
			noise.get([(pos.y * noise_scale) as f64, (pos.x * noise_scale) as f64]) as f32 * noise_strength
		));
	}

	fn draw(&self, draw: &Draw, noise: Perlin, noise_scale: f32, noise_strength: f32, t: f32) {

        let points = (0..self.path.len()).map(|i|{
            let point = self.path[i];
            let index = noise.get([
                (point.x*noise_scale) as f64,
                (point.y*noise_scale) as f64, 
                (i as f32 +t) as f64 * noise_scale as f64]) 
                * (self.color.len() - 1)as f64;
            let hue = self.color[index.round() as usize];

            (point, hue)
        });

		draw.polyline()
			.weight(self.width)
			.end_cap_square()
			.join_round()
            .points_colored(points);
	}
}
use nannou::noise::{NoiseFn, Perlin};
use nannou::prelude::*;

struct Model {}

fn main() {
    nannou::app(model).update(update).simple_window(view).run();
}

fn model(app: &App) -> Model {
    Model {}
}

fn update(app: &App, model: &mut Model, update: Update) {}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();
    let t = app.time;
    let win = app.window_rect();

    draw.background().color(Rgb::new(0.75, 0.75, 0.75));

    for i in 0..720 {
        let rad = i as f32 * PI / 180.0;
        let x = rad.cos();
        let y = rad.sin();
        let ring_size = 180.0;

        let noise = Perlin::new();
        let size = noise.get([x as f64, y as f64, (t * 0.8) as f64]) as f32;

        draw.ellipse()
            .x_y(x * ring_size, y * ring_size)
            .radius(size * 45.0 + 24.0)
            .rgb(
                ((rad + t).sin() + 1.0) / 2.0,
                ((rad + t).cos() + 1.0) / 2.0,
                ((rad - t).sin() + 1.0) / 2.0,
            );
    }

    draw.to_frame(app, &frame).unwrap();
}

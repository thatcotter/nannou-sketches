use nannou::prelude::*;

struct Model {}

fn main() {
    nannou::app(model).update(update).simple_window(view).run();
}

fn model(_app: &App) -> Model {
    Model {}
}

fn update(_app: &App, _model: &mut Model, _update: Update) {}

fn view(app: &App, _model: &Model, frame: Frame) {
    let draw = app.draw();
    let _t = app.time;

    draw.background().color(Rgb::new(0.0, 0.0, 0.0));

    draw.to_frame(app, &frame).unwrap();
}

use nannou::app::DrawScalar;
use nannou::noise::*;
use nannou::prelude::*;

// use helpers::lerp_colors;

struct Model {}

fn main() {
    nannou::app(model).update(update).simple_window(view).run();
}

fn model(_app: &App) -> Model {
    Model {}
}

fn update(_app: &App, _model: &mut Model, _update: Update) {}

fn view(app: &App, _model: &Model, frame: Frame) {
    let draw = app.draw();
    let t = app.time;
    let win = app.window_rect();

    draw.background().color(BLACK);

    let color_a = rgba(15.0 / 255.0, 76.0 / 255.0, 117.0 / 255.0, 1.0);
    let color_b = rgba(0.0 / 255.0, 183.0 / 255.0, 194.0 / 255.0, 1.0);

    let color_c = rgba(253.0 / 255.0, 203.0 / 255.0, 158.0 / 255.0, 1.0);
    let color_d = rgba(117.0 / 255.0, 95.0 / 255.0, 76.0 / 255.0, 1.0);

    let num_rows = 10;

    // let draw = draw.translate(geom::pt3(100.0, 0.0, 0.0));
    let draw = draw.scale(2.0);
    let draw = draw.radians(pt3(0.0, 0.0, 1.0));

    for i in 0..=(num_rows / 2) {
        let height = map_f32(
            i as f32,
            0.0,
            num_rows as f32 / 2.0,
            win.top() + 50.0,
            win.top() - 200.0,
        );
        let pct: f32 = map_f32(i as f32, 0.0, num_rows as f32, 0.0, 1.0);
        let color = lerp_colors(color_c, color_d, pct);
        draw.polygon()
            .color(color)
            .stroke_weight(0.0)
            .points(soft_wave_points(height, 50.0, t * 0.05, win));
    }

    for i in 0..=num_rows {
        let height = map_f32(
            i as f32,
            0.0,
            num_rows as f32,
            win.top() - 200.0,
            win.bottom(),
        );
        let pct: f32 = map_f32(i as f32, 0.0, num_rows as f32, 0.0, 1.0);
        let color = lerp_colors(color_b, color_a, pct);
        draw.polygon()
            .color(color)
            .stroke_weight(0.0)
            .points(soft_wave_points(height, 50.0, t, win));
    }

    draw.to_frame(app, &frame).unwrap();
}

fn soft_wave_points(
    y: f32,
    amp: f32,
    time: f32,
    window: geom::Rect<DrawScalar>,
) -> Vec<geom::Point2> {
    let noise = Perlin::new();
    let points: Vec<geom::Point2> = (0..=100)
        .map(|i| {
            let x = map_f32(i as f32, 0.0, 100.0, window.left(), window.right());
            let y = y
                + ((x + time).sin()
                    + (noise.get([
                        0.005 * x as f64,
                        0.005 * y as f64,
                        time as f64,
                        (time * 1.5) as f64,
                    ]) as f32)
                        .sin()
                    + (y + time * 2.0).cos())
                    / 3.0
                    * amp;
            pt2(x as f32, y as f32)
        })
        .collect();

    let mut result: Vec<geom::Point2> = Vec::new();
    result.push(pt2(window.left(), window.bottom()));
    for point in points {
        result.push(point);
    }
    result.push(pt2(window.right(), window.bottom()));

    result
}

fn map_f32(value: f32, istart: f32, istop: f32, ostart: f32, ostop: f32) -> f32 {
    return ostart + (ostop - ostart) * ((value - istart) / (istop - istart));
}

fn lerp_colors(a: Rgba, b: Rgba, pct: f32) -> Rgba {
    let r = a.red + (b.red - a.red) * pct;
    let g = a.green + (b.green - a.green) * pct;
    let b = a.blue + (b.blue - a.blue) * pct;
    let a = 1.0;
    Rgba::new(r, g, b, a)
}

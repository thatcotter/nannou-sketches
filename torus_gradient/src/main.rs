use nannou::prelude::*;

struct Model {}

fn main() {
    nannou::app(model).update(update).simple_window(view).run();
}

fn model(_app: &App) -> Model {
    Model {}
}

fn update(_app: &App, _model: &mut Model, _update: Update) {}

fn view(app: &App, _model: &Model, frame: Frame) {
    let draw = app.draw();
    let t = app.time;
    let win = app.window_rect();

    let inner_radius = win.top() / 2.0;
    let torus_width = 100.0;

    draw.background().color(Rgb::new(0.85, 0.85, 0.85));

    for i in 0..360 {
        let sine_norm = (t + i as f32).sin() + 1.0 * 0.5;
        let length = sine_norm * torus_width + inner_radius;

        let x = (i as f32 / 180.0 * PI).cos() * length;
        let y = (i as f32 / 180.0 * PI).sin() * length;

        let r = 0.9 - sine_norm * 0.85;
        let g = 0.0;
        let b = 0.33;

        draw.ellipse()
            .rgb(r, g, b)
            .x_y(x, y)
            .radius(5.0 - sine_norm * 3.0);
    }

    draw.to_frame(app, &frame).unwrap();
}

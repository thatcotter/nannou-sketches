use nannou::noise::{NoiseFn, Perlin};
use nannou::prelude::*;

struct Model {}

#[derive(Debug)]
struct Blob {
    points: Vec<Point2>,
    colors: Vec<Rgba>,
}

impl Blob {
    fn new_empty() -> Blob {
        Blob {
            points: Vec::new(),
            colors: Vec::new(),
        }
    }

    fn new(center: Point2, radius: f32, noise_amp: f32, time: f32, iteration: i32) -> Blob {
        let noise = Perlin::new();
        let mut blob = Blob::new_empty();
        let n = 360;
        let values: Vec<(Point2, Rgba)> = (0..=n)
            .map(|i| {
                let i = i as f32;
                let radian = deg_to_rad(i);
                let offset = noise.get([radian.cos() as f64, radian.sin() as f64, time as f64])
                    as f32
                    * noise_amp;
                let x = radian.sin() * (radius + offset);
                let y = radian.cos() * (radius + offset);
                let r = 0.15
                    + noise.get([
                        (x * 0.001) as f64,
                        (y * 0.001) as f64,
                        i as f64,
                        time as f64 / 2.0 + 0.0,
                    ]) as f32;
                let g = 0.15
                    + noise.get([
                        (x * 0.001) as f64,
                        (y * 0.001) as f64,
                        i as f64,
                        time as f64 / 2.0 + 100.0,
                    ]) as f32;
                let b = 0.15
                    + noise.get([
                        (x * 0.001) as f64,
                        (y * 0.001) as f64,
                        i as f64,
                        time as f64 / 2.0 + 200.0,
                    ]) as f32;
                (center + pt2(x, y), rgba(r, g, b, 0.14))
            })
            .collect();
        for value in values {
            blob.points.push(value.0);
            blob.colors.push(value.1);
        }
        blob
    }

    fn nested_blobs(
        n_steps: i32,
        center: Point2,
        min_radius: f32,
        radius_step: f32,
        min_noise_amp: f32,
        amp_step: f32,
        time: f32,
    ) -> Vec<Blob> {
        let blobs = (0..n_steps)
            .map(|i| {
                Blob::new(
                    center,
                    min_radius + radius_step * i as f32,
                    min_noise_amp + amp_step * i as f32,
                    time + i as f32 * 0.03,
                    i,
                )
            })
            .collect();
        blobs
    }

    fn points(&self) -> Vec<Point2> {
        self.points.clone()
    }

    fn colors(&self) -> Vec<Rgba> {
        self.colors.clone()
    }

    fn colored_points(&self) -> Vec<(Point2, Rgba)> {
        let mut result = Vec::new();
        for i in 0..self.points.len() {
            result.push((self.points[i], self.colors[i]))
        }
        result
    }
}

fn main() {
    nannou::app(model).update(update).view(view).run();
}

fn model(app: &App) -> Model {
    app.new_window().size(1200, 900).build().unwrap();
    Model {}
}

fn update(_app: &App, _model: &mut Model, _update: Update) {}

fn view(app: &App, _model: &Model, frame: Frame) {
    let draw = app.draw();
    let t = app.time;

    frame.clear(rgb(0.7, 0.7, 0.7));
    let draw = draw.color_blend(BLEND_NORMAL);

    let blobs = Blob::nested_blobs(21, pt2(0.0, 0.0), 20.0, 20.0, 0.0, 2.75, t);
    let mut i = 0;
    for blob in blobs {
        draw.polygon().color(blob.colors()[i]).points(blob.points());
        // draw.polyline()
        //     .weight(3.0)
        //     .points_colored(blob.colored_points());
        i += 1;
    }

    draw.to_frame(app, &frame).unwrap();
}

use nannou::prelude::*;
mod Fluid;

struct Model {
    p_mouse: Point2,
    fluid: Fluid::Fluid,
    drawing: bool,
}

fn main() {
    nannou::app(model).update(update).run();
}

fn model(app: &App) -> Model {
    let n: i32 = 128;
    let scale: i32 = 4;
    let iter = 16;
    let window_size = (&n * &scale) as u32;

    app.new_window()
        // .with_dimensions(window_size, window_size)
        .size(window_size, window_size)
        .event(event)
        .view(view)
        .build()
        .unwrap();
    let p_mouse = pt2(0.0, 0.0);
    let fluid = Fluid::Fluid::new(n, scale, iter, 0.2, 0.0, 0.0000001);
    let drawing = false;
    Model {
        p_mouse,
        fluid,
        drawing,
    }
}

fn update(_app: &App, model: &mut Model, _update: Update) {
    model.fluid.step();
    model.fluid.fade_d();
}

fn event(_app: &App, model: &mut Model, event: WindowEvent) {
    match event {
        MouseMoved(_pos) => {
            if model.drawing {
                let scale = model.fluid.scale as f32;
                let size = model.fluid.size as f32;
                model.fluid.add_density(
                    (_pos.x / scale + size / 2.0) as i32,
                    (_pos.y / scale + size / 2.0) as i32,
                    0.9,
                );

                let amt_x = _pos.x - model.p_mouse.x;
                let amt_y = _pos.y - model.p_mouse.y;
                model.fluid.add_velocity(
                    (_pos.x / scale + size / 2.0) as i32,
                    (_pos.y / scale + size / 2.0) as i32,
                    amt_x,
                    amt_y,
                );
            }
            model.p_mouse = _pos;
        }
        MousePressed(_button) => model.drawing = true,
        MouseReleased(_button) => model.drawing = false,
        _ => {}
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();
    let _t = app.time;
    let win = app.window_rect();

    draw.background().rgb(0.15, 0.15, 0.15);

    model.fluid.render_d(&draw, &win);
    // model.fluid.render_v(&draw);

    draw.to_frame(app, &frame).unwrap();
}

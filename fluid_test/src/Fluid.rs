use nannou::app::DrawScalar;
use nannou::prelude::*;

fn ix(x: i32, y: i32, size: i32) -> usize {
    let x = x.max(0).min(size - 1);
    let y = y.max(0).min(size - 1);
    (x + (y * size)) as usize
}

#[derive(Debug)]
pub struct Fluid {
    pub size: i32,
    pub scale: i32,
    pub iter: i32,
    dt: f32,
    diff: f32,
    visc: f32,
    s: Vec<f32>,
    density: Vec<f32>,
    vx: Vec<f32>,
    vy: Vec<f32>,
    vx0: Vec<f32>,
    vy0: Vec<f32>,
}

impl Fluid {
    pub fn new(size: i32, scale: i32, iter: i32, dt: f32, diff: f32, visc: f32) -> Fluid {
        let mut vec_len = Vec::new();
        vec_len.resize((size * size) as usize, 0.0);

        let s = (&vec_len[..]).to_vec();
        let density = (&vec_len[..]).to_vec();
        let vx = (&vec_len[..]).to_vec();
        let vy = (&vec_len[..]).to_vec();
        let vx0 = (&vec_len[..]).to_vec();
        let vy0 = (&vec_len[..]).to_vec();

        Fluid {
            size,
            scale,
            iter,
            dt,
            diff,
            visc,
            s,
            density,
            vx,
            vy,
            vx0,
            vy0,
        }
    }

    pub fn step(&mut self) {
        let n = self.size;
        let iter = self.iter;
        let visc = self.visc;
        let diff = self.diff;
        let dt = self.dt;
        let mut vx = (&self.vx).to_vec();
        let mut vy = (&self.vy).to_vec();
        let mut vx0 = (&self.vx0).to_vec();
        let mut vy0 = (&self.vy0).to_vec();
        let mut s = (&self.s).to_vec();
        let mut density = (&self.density).to_vec();

        // diffuse(1, Vx0, Vx, visc, dt);
        // diffuse(2, Vy0, Vy, visc, dt);

        // project(Vx0, Vy0, Vx, Vy);

        // advect(1, Vx, Vx0, Vx0, Vy0, dt);
        // advect(2, Vy, Vy0, Vx0, Vy0, dt);

        // project(Vx, Vy, Vx0, Vy0);

        // diffuse(0, s, density, diff, dt);
        // advect(0, density, s, Vx, Vy, dt);

        diffuse(n, iter, 1, &mut self.vx0, &vx, visc, dt);
        vx0 = (&self.vx0).to_vec();

        diffuse(n, iter, 2, &mut self.vy0, &vy, visc, dt);
        vy0 = (&self.vy0).to_vec();

        // project(vx0, vy0, vx, vy, iter);
        project(
            n,
            iter,
            &mut self.vx0,
            &mut self.vy0,
            &mut self.vx,
            &mut self.vy,
        );
        vx = (&self.vx).to_vec();
        vy = (&self.vy).to_vec();
        vx0 = (&self.vx0).to_vec();
        vy0 = (&self.vy0).to_vec();

        advect(n, 1, &mut self.vx, &vx0, &vx0, &vy0, &dt);
        vx = (&self.vx).to_vec();

        advect(n, 2, &mut self.vy, &vy0, &vx0, &vy0, &dt);
        // vx = (&self.vx).to_vec();
        vy = (&self.vy).to_vec();
        // vx0 = (&self.vx0).to_vec();
        // vy0 = (&self.vy0).to_vec();
        // s = (&self.s).to_vec();
        // density = (&self.density).to_vec();

        project(
            n,
            iter,
            &mut self.vx,
            &mut self.vy,
            &mut self.vx0,
            &mut self.vy0,
        );
        vx = (&self.vx).to_vec();
        vy = (&self.vy).to_vec();
        vx0 = (&self.vx0).to_vec();
        vy0 = (&self.vy0).to_vec();
        // s = (&self.s).to_vec();
        // density = (&self.density).to_vec();

        diffuse(n, iter, 0, &mut self.s, &density, diff, dt);
        // vx = (&self.vx).to_vec();
        // vy = (&self.vy).to_vec();
        // vx0 = (&self.vx0).to_vec();
        // vy0 = (&self.vy0).to_vec();
        s = (&self.s).to_vec();
        // density = (&self.density).to_vec();

        // diffuse(n, iter, 0, &mut self.density, &s, diff, dt);
        // vx = (&self.vx).to_vec();
        // vy = (&self.vy).to_vec();
        // vx0 = (&self.vx0).to_vec();
        // vy0 = (&self.vy0).to_vec();
        // s = (&self.s).to_vec();
        // density = (&self.density).to_vec();

        advect(n, 0, &mut self.density, &s, &vx, &vy, &dt);
        // vx = (&self.vx).to_vec();
        // vy = (&self.vy).to_vec();
        // vx0 = (&self.vx0).to_vec();
        // vy0 = (&self.vy0).to_vec();
        // s = (&self.s).to_vec();
        // density = (&self.density).to_vec();
    }

    pub fn add_density(&mut self, x: i32, y: i32, amount: f32) {
        let index = ix(x, y, self.size);
        self.density[index] += amount;
    }

    pub fn add_velocity(&mut self, x: i32, y: i32, amount_x: f32, amount_y: f32) {
        let index = ix(x, y, self.size);
        self.vx[index] += amount_x;
        self.vy[index] += amount_y;
    }

    //render the density
    pub fn render_d(&self, draw: &Draw, _win: &geom::Rect<DrawScalar>) {
        for i in 0..self.size {
            for j in 0..self.size {
                let x = (i * self.scale) as f32 - (self.size * self.scale) as f32 * 0.5;
                let y = (j * self.scale) as f32 - (self.size * self.scale) as f32 * 0.5;
                let d = self.density[ix(i, j, self.size)];

                draw.rect()
                    .x_y(x, y)
                    .w_h(self.scale as f32, self.scale as f32)
                    // .rgb(d, d, d);
                    .hsv((d + 0.5) % 1.0, 0.8, d);
            }
        }
    }

    pub fn render_v(&self, draw: &Draw) {
        for i in 0..self.size {
            for j in 0..self.size {
                let x = (i - self.size / 2) * self.scale;
                let y = (j - self.size / 2) * self.scale;
                let vx = self.vx[ix(i, j, self.size)];
                let vy = self.vy[ix(i, j, self.size)];

                if !(vx.abs() < 0.1 && vy.abs() <= 0.1) {
                    draw.line()
                        .start(pt2(x as f32, y as f32))
                        .end(pt2(
                            x as f32 + vx * self.scale as f32,
                            y as f32 + vy * self.scale as f32,
                        ))
                        .color(WHITE);
                }
            }
        }
    }

    pub fn fade_d(&mut self) {
        for (i, d) in (self.density.to_vec()).iter().enumerate() {
            self.density[i] = (d - 0.0002).max(0.0).min(255.0);
        }
    }
}

/*
    Function of diffuse
    - b : int
    - x : float[]
    - x0 : float[]
    - diff : float
    - dt : flaot
*/
fn diffuse(n: i32, iter: i32, b: i32, x: &mut Vec<f32>, x0: &Vec<f32>, diff: f32, dt: f32) {
    let a = dt * diff * ((n - 2) * (n - 2)) as f32;
    lin_solve(b, n, iter, x, x0, a, 1.0 + 6.0 * a);
}

/*
    Function of solving linear differential equation
    - b : int
    - x : float[]
    - x0 : float[]
    - a : float
    - c : float
*/
fn lin_solve(b: i32, n: i32, iter: i32, x: &mut Vec<f32>, x0: &Vec<f32>, a: f32, c: f32) {
    let c_recip: f32 = 1.0 / c;

    for _ in 0..iter {
        for j in 1..n - 1 {
            for i in 1..n - 1 {
                x[ix(i, j, n)] = (x0[ix(i, j, n)]
                    + a * (&x[ix(i + 1, j, n)]
                        + &x[ix(i - 1, j, n)]
                        + &x[ix(i, j + 1, n)]
                        + &x[ix(i, j - 1, n)]))
                    * c_recip;
            }
        }
        set_bnd(n, b, x);
    }
}

/*
    Function of project : This operation runs through all the cells and fixes them up so everything is in equilibrium.
    - velocX : float[]
    - velocY : float[]
    = p : float[]
    - div : float[]
*/
fn project(
    n: i32,
    iter: i32,
    veloc_x: &mut Vec<f32>,
    veloc_y: &mut Vec<f32>,
    p: &mut Vec<f32>,
    div: &mut Vec<f32>,
) {
    for j in 1..n - 1 {
        for i in 1..n - 1 {
            div[ix(i, j, n)] = -0.5
                * (veloc_x[ix(i + 1, j, n)] - veloc_x[ix(i - 1, j, n)] + veloc_y[ix(i, j + 1, n)]
                    - veloc_y[ix(i, j - 1, n)])
                / n as f32;
            p[ix(i, j, n)] = 0.0;
        }
    }
    set_bnd(n, 0, div);
    set_bnd(n, 0, p);
    lin_solve(0, n, iter, p, div, 1.0, 6.0);

    for j in 1..n - 1 {
        for i in 1..n - 1 {
            veloc_x[ix(i, j, n)] -= 0.5 * (p[ix(i + 1, j, n)] - p[ix(i - 1, j, n)]) * n as f32;
            veloc_y[ix(i, j, n)] -= 0.5 * (p[ix(i, j + 1, n)] - p[ix(i, j - 1, n)]) * n as f32;
        }
    }
    set_bnd(n, 1, veloc_x);
    set_bnd(n, 2, veloc_y);
}

fn advect(
    n: i32,
    b: i32,
    d: &mut Vec<f32>,
    d0: &Vec<f32>,
    veloc_x: &Vec<f32>,
    veloc_y: &Vec<f32>,
    dt: &f32,
) {
    let n_float = n as f32;

    let dtx = dt * (n_float - 2.0);
    let dty = dt * (n_float - 2.0);

    for j in 1..n - 1 {
        for i in 1..n - 1 {
            let tmp1 = dtx * veloc_x[ix(i, j, n)];
            let tmp2 = dty * veloc_y[ix(i, j, n)];
            let mut x = i as f32 - tmp1;
            let mut y = j as f32 - tmp2;

            x = x.max(0.5);
            x = x.min(n_float + 0.5);
            let i0 = x.floor() as f32;
            let i1 = i0 + 1.0;

            y = y.max(0.5);
            y = y.min(n_float + 0.5);
            let j0 = y.floor();
            let j1 = j0 + 1.0;

            let s1 = x - i0;
            let s0 = 1.0 - s1;
            let t1 = y - j0;
            let t0 = 1.0 - t1;

            let i0i = i0 as i32;
            let i1i = i1 as i32;
            let j0i = j0 as i32;
            let j1i = j1 as i32;

            // DOUBLE CHECK THIS!!!
            d[ix(i, j, n)] = s0 * (t0 * d0[ix(i0i, j0i, n)] + t1 * d0[ix(i0i, j1i, n)])
                + s1 * (t0 * d0[ix(i1i, j0i, n)] + t1 * d0[ix(i1i, j1i, n)]);
        }
    }

    set_bnd(n, b, d);
}

fn set_bnd(n: i32, b: i32, x: &mut Vec<f32>) {
    for i in 1..(n - 1) {
        x[ix(i, 0, n)] = if b == 2 {
            -x[ix(i, 1, n)]
        } else {
            x[ix(i, 1, n)]
        };
        x[ix(i, n - 1, n)] = if b == 2 {
            -x[ix(i, n - 2, n)]
        } else {
            x[ix(i, n - 2, n)]
        };
    }

    for j in 1..(n - 1) {
        x[ix(0, j, n)] = if b == 1 {
            -x[ix(1, j, n)]
        } else {
            x[ix(1, j, n)]
        };
        x[ix(n - 1, j, n)] = if b == 1 {
            -x[ix(n - 2, j, n)]
        } else {
            x[ix(n - 2, j, n)]
        };
    }

    x[ix(0, 0, n)] = 0.5 * (x[ix(1, 0, n)] + x[ix(0, 1, n)]);
    x[ix(0, n - 1, n)] = 0.5 * (x[ix(1, n - 1, n)] + x[ix(0, n - 2, n)]);
    x[ix(n - 1, 0, n)] = 0.5 * (x[ix(n - 2, 0, n)] + x[ix(n - 1, 1, n)]);
    x[ix(n - 1, n - 1, n)] = 0.5 * (x[ix(n - 2, n - 1, n)] + x[ix(n - 1, n - 2, n)]);
}

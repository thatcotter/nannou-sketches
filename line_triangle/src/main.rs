use draw_helpers::line_triangle;
use nannou::prelude::*;

struct Model {}

fn main() {
    nannou::app(model).run();
}

fn model(_app: &App) -> Model {
    _app.new_window().event(event).view(view).build().unwrap();
    Model {}
}

fn update(_app: &App, _model: &mut Model, _update: Update) {}

fn view(_app: &App, model: &Model, frame: Frame) {
    let draw = _app.draw();
    let t = _app.time;
    let win = _app.window_rect();

    draw.background().color(Rgb::new(0.02, 0.02, 0.02));

    line_triangle(
        &_app,
        &draw,
        pt2(0.0, 0.0),
        32,
        120.0,
        (t * 0.5).sin() * PI * 0.125,
    );

    draw.to_frame(_app, &frame).unwrap();
}

fn event(_app: &App, _model: &mut Model, event: WindowEvent) {
    match event {
        MousePressed(_button) => print!("{:?}", _app.mouse),
        _other => {}
    }
}

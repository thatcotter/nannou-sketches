extern crate nannou;
use nannou::image::imageops::{dither, ColorMap};
use nannou::ui::Dimensions;
use nannou::ui::color::rgb_bytes;
use nannou::{prelude::*};
use nannou::image::{self, DynamicImage, GenericImage, Rgb};
use nannou::image::GenericImageView;

struct Model {
    // texture: wgpu::Texture,
	image: image::DynamicImage,
	// points: Vec<Vector2>,
	// colors: Vec<Rgb>
}

fn main() {
    nannou::app(model).update(update).run();
}

fn model(app: &App) -> Model {
	app.set_loop_mode(LoopMode::loop_once());

	let assets = app.assets_path().unwrap();
    let img_path = assets
        .join("images")
        .join("selfie_mini.jpg");
    // let texture = wgpu::Texture::from_path(app, &img_path).unwrap();
	let image = image::open(img_path).unwrap();

	app.new_window()
		.size(image.width(), image.height())
        // .event(event)
        .view(view)
        .build()
        .unwrap();

    Model {
		image
	}
}

fn update(_app: &App, _model: &mut Model, _update: Update) {
}

fn view(app: &App, model: &Model, frame: Frame){
	frame.clear(BLACK);

	let draw = app.draw();
	let win = app.window_rect();
	let tile_count_x = map_range(app.mouse.x, win.left(), win.right(), 1.0, win.w() / 3.0);
    let tile_count_y = map_range(app.mouse.y, win.top(), win.bottom(), 1.0, win.h() / 3.0);
    // let step_x = win.w() / tile_count_x;
    // let step_y = win.h() / tile_count_y;

	// let mouse_x_factor = map_range(app.mouse.x, win.left(), win.right(), 0.01, 1.0);
    // let mouse_y_factor = map_range(app.mouse.y, win.bottom(), win.top(), 0.01, 1.0);

	let mut dithered = model.image.clone();

	let (w, h) = model.image.dimensions();
	for grid_y in 1..h-1 {
    	for grid_x in 1..w-1 {
            // get current color
            let c = model.image.get_pixel(grid_x, grid_y);
            // greyscale conversion
			let factor = 3.0;
            let red   = (factor * c[0] as f32 / 255.0).round() * (1.0/factor);
            let green = (factor * c[1] as f32 / 255.0).round() * (1.0/factor);
            let blue  = (factor * c[2] as f32 / 255.0).round() * (1.0/factor);
            let greyscale = red * 0.222 + green * 0.707 + blue * 0.071;

			let errR = c[0] as f32 / 255.0 - red;
			let errG = c[1] as f32 / 255.0 - green;
			let errB = c[2] as f32 / 255.0 - blue;
			let err_pixel = rgb(errR, errG, errB);
		
			quant_error(&mut dithered, err_pixel, grid_x, grid_y, Point2::new(w, h));
			
			let tile_width = win.w() / w as f32;
            let tile_height = win.h() / h as f32;
            let pos_x = win.left() + tile_width * grid_x as f32 + (tile_width / 2.0);
            let pos_y = win.top() - tile_height * grid_y as f32 - (tile_height / 2.0);

			let hue = dithered.get_pixel(grid_x, grid_y);
			let newR = hue[0] as f32;
			let newG = hue[1] as f32;
			let newB = hue[2] as f32;

			draw.rect()
				.x_y(pos_x, pos_y)
				.w_h(3.5, 3.5)
				.color(rgb(newR,newG,newB));
				// .color(rgb(greyscale, greyscale, greyscale));

		}
	}


	draw.to_frame(app, &frame).unwrap();
}

fn quant_error(image: &mut DynamicImage, error: nannou::color::rgb::Rgb, x: u32, y: u32, dims: Vector2<u32>){
	// 	pixels[x + 1][y    ] := pixels[x + 1][y    ] + quant_error × 7 / 16
    //  pixels[x - 1][y + 1] := pixels[x - 1][y + 1] + quant_error × 3 / 16
    //  pixels[x    ][y + 1] := pixels[x    ][y + 1] + quant_error × 5 / 16
    //  pixels[x + 1][y + 1] := pixels[x + 1][y + 1] + quant_error × 1 / 16
	
	let mut c1 = image.get_pixel(x+1.min(dims.x-1), y);
	c1[0] = ((c1[0] + (error.red * 255.0) as u8) as f32 * 7.0 / 16.0) as u8;
	c1[1] = ((c1[1] + (error.green * 255.0) as u8) as f32 * 7.0 / 16.0) as u8;
	c1[2] = ((c1[2] + (error.blue * 255.0) as u8) as f32 * 7.0 / 16.0) as u8;
	image.put_pixel(x+1, y, c1);

	let mut c2 = image.get_pixel(x-1.max(0), y+1.min(dims.y-1));
	c2[0] = ((c2[0] + (error.red * 255.0) as u8) as f32 * 3.0 / 16.0) as u8;
	c2[1] = ((c2[1] + (error.green * 255.0) as u8) as f32 * 3.0 / 16.0) as u8;
	c2[2] = ((c2[2] + (error.blue * 255.0) as u8) as f32 * 3.0 / 16.0) as u8;
	image.put_pixel(x-1, y+1, c2);

	let mut c3 = image.get_pixel(x, y+1.min(dims.y-1));
	c3[0] = ((c3[0] + (error.red * 255.0) as u8) as f32 * 5.0 / 16.0) as u8;
	c3[1] = ((c3[1] + (error.green * 255.0) as u8) as f32 * 5.0 / 16.0) as u8;
	c3[2] = ((c3[2] + (error.blue * 255.0) as u8) as f32 * 5.0 / 16.0) as u8;
	image.put_pixel(x, y+1, c3);

	let mut c4 = image.get_pixel(x+1.min(dims.x-1), y);
	c4[0] = ((c4[0] + (error.red * 255.0) as u8) as f32 * 1.0 / 16.0) as u8;
	c4[1] = ((c4[1] + (error.green * 255.0) as u8) as f32 * 1.0 / 16.0) as u8;
	c4[2] = ((c4[2] + (error.blue * 255.0) as u8) as f32 * 1.0 / 16.0) as u8;
	image.put_pixel(x+1, y, c4);
}
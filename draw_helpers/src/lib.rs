use helpers::lerp_colors;
use nannou::prelude::*;
use nannou::Draw;

pub fn line_circle(
    app: &App,
    draw: &Draw,
    pos: Point2,
    resolution: i32,
    radius: f32,
    theta: f32,
) {
    let t = app.time;
    let step = 180.0 / resolution as f32;
    let bound = resolution / 2;

    let top_color = rgba(0.91372549, 0.329411765, 0.125490196, 1.0);
    let bot_color = rgba(0.466666667, 0.129411765, 0.435294118, 1.0);

    for i in -bound..bound {
        let angle = i as f32 * step * PI / 180.0;
        let opposite = PI - angle;
        let wave_radius = radius * ((angle + t).sin() * 0.25);
        let gap = radius / 4.0;

        let points1 = vec![
            pt2(radius * angle.sin(), radius * angle.cos()),
            pt2(
                radius * opposite.sin(),
                (wave_radius - gap) * opposite.cos(),
            ),
        ];

        let points2 = vec![
            pt2(radius * opposite.sin(), radius * opposite.cos()),
            pt2(
                radius * opposite.sin(),
                (wave_radius + gap) * opposite.cos(),
            ),
        ];

        let line1 = (0..2).map(|i| points1[i]).enumerate().map(|(_i, p)| {
            let pct = (p.y / radius * 1.25).abs();
            let rgba = lerp_colors(top_color, bot_color, pct);
            (p, rgba)
        });

        let line2 = (0..2).map(|i| points2[i]).enumerate().map(|(_i, p)| {
            let pct = (p.y / radius * 1.25).abs();
            let rgba = lerp_colors(top_color, bot_color, pct);
            (p, rgba)
        });

        draw.polyline().points_colored(line1).xy(pos).rotate(theta);
        draw.polyline().points_colored(line2).xy(pos).rotate(theta);
    }
}

pub fn line_square(
    app: &App,
    draw: &Draw,
    pos: Point2,
    resolution: i32,
    size: f32,
    theta: f32,
) {
    let t = app.time;
    let step = size / resolution as f32;
    let bound = resolution / 2;

    let top_color = rgba(0.91372549, 0.329411765, 0.125490196, 1.0);
    let bot_color = rgba(0.466666667, 0.129411765, 0.435294118, 1.0);

    for i in -bound..bound {
        let angle = i as f32 * step * PI / 180.0;
        let wave = size * ((angle + t).round().sin() * 0.33);
        let gap = size / 4.0;

        let x = i as f32 * step * 2.0;
        let y = size;

        let points1 = vec![pt2(x, y), pt2(x, wave + gap)];

        let points2 = vec![pt2(x, -y), pt2(x, wave - gap)];

        let line1 = (0..2).map(|i| points1[i]).enumerate().map(|(_i, p)| {
            let pct = (p.y / size * 1.25).abs();
            let rgba = lerp_colors(top_color, bot_color, pct);
            (p, rgba)
        });

        let line2 = (0..2).map(|i| points2[i]).enumerate().map(|(_i, p)| {
            let pct = (p.y / size * 1.25).abs();
            let rgba = lerp_colors(top_color, bot_color, pct);
            (p, rgba)
        });

        draw.polyline().points_colored(line1).xy(pos).rotate(theta);
        draw.polyline().points_colored(line2).xy(pos).rotate(theta);
    }
}

pub fn line_triangle(
    app: &App,
    draw: &Draw,
    pos: Point2,
    resolution: i32,
    size: f32,
    theta: f32,
) {
    let t = app.time;
    let step = size / resolution as f32 * 2.5;
    let bound = resolution / 2;

    let top_color = rgba(0.91372549, 0.329411765, 0.125490196, 1.0);
    let bot_color = rgba(0.466666667, 0.129411765, 0.435294118, 1.0);

    for i in -bound..bound {
        let angle = i as f32 * step * PI / 180.0 * 2.0;
        let wave = size * ((angle + t).sin().acos() / 1.5708 * 0.66) - size * 0.33;
        let gap = size / 4.0;

        let x = i as f32 * step * 2.0 + size * 0.85;
        let y = size - i as f32 * step * 1.125;

        let points1 = vec![pt2(x, y), pt2(x, (wave + gap).max(-y).min(y))];

        let points2 = vec![pt2(x, -y), pt2(x, (wave - gap).min(y).max(-y))];

        let line1 = (0..2).map(|i| points1[i]).enumerate().map(|(_i, p)| {
            let pct = (p.y / size * 0.8).abs();
            let rgba = lerp_colors(top_color, bot_color, pct);
            (p, rgba)
        });

        let line2 = (0..2).map(|i| points2[i]).enumerate().map(|(_i, p)| {
            let pct = (p.y / size * 0.8).abs();
            let rgba = lerp_colors(top_color, bot_color, pct);
            (p, rgba)
        });

        draw.polyline().points_colored(line1).xy(pos).rotate(theta);
        draw.polyline().points_colored(line2).xy(pos).rotate(theta);
    }
}

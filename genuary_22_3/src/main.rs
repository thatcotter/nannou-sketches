extern crate nannou;
use nannou::lyon::path::Position;
use nannou::{prelude::*};
use nannou::rand::random_range;

struct Star {
	position: Vector2,
	num_connections: i32,
	id: usize
}

impl Star {
	fn new(x:f32,y:f32, id: usize) -> Star {
		Star {
			position: Vector2::new(x,y),
			num_connections: 0,
			id
		}
	}

	fn plot(&mut self, theta: f32, mag: f32) {
		self.position = Point2::new(theta.cos() * mag, theta.sin() * mag)
	}

	fn draw(&self, draw: Draw) {
		unimplemented!()
	}
}

struct StarConnection {
	start: usize,
	end: usize
}

impl StarConnection {
	fn new(start: usize, end: usize) -> StarConnection {
		StarConnection {
			start,
			end
		}
	}

	fn get_points(&self, model: Model) -> (Vector2, Vector2) {
		(model.stars[self.start].position, model.stars[self.end].position)
	}
}

struct Model {
    stars: Vec<Star>,
	connections: Vec<StarConnection>
}

fn main() {
    nannou::app(model).update(update).run();
}



fn model(app: &App) -> Model {
	app.new_window()
        .view(view)
        .build()
        .unwrap();
	// app.set_loop_mode(LoopMode::loop_once());

	let mut stars: Vec<Star> = Vec::new();
	let mut connections = Vec::new();

	let connection_dist = 1700.0;

	for i in 0..5000 {
		let mut temp = Star::new(0.0, 0.0, i as usize);
		temp.plot(random_range(0.0,TAU), random_range(25.0, 3000.0));
		replot_if_too_close(&mut temp, &stars);

		for star in &mut stars {
			if temp.position.distance2(star.position) < connection_dist {
				if temp.num_connections < 3 && star.num_connections < 3 {
					connections.push(StarConnection::new(star.id, temp.id));
					star.num_connections += 1;
					temp.num_connections += 1;
				}
			}
		}

		stars.push(temp);
	}

    Model {
		stars,
		connections
	}
}

fn update(_app: &App, _model: &mut Model, _update: Update) {
}

fn view(app: &App, model: &Model, frame: Frame){
	frame.clear(rgb(0.08, 0.05, 0.1));

	let draw = app.draw();
	let win = app.window_rect();
	let t = app.time;

	let translation = Vector3::new(win.right() * 1.2, win.bottom() * 0.8, 0.0);
	
	let draw2 = draw.translate(translation).rotate(t/180.0*PI);

	for connection in &model.connections {
		draw2.line()
			.start(model.stars[connection.start].position)
			.end(model.stars[connection.end].position)
			.color(WHITE);
	}

	for star in &model.stars {
		draw2.ellipse()
			.xy(star.position)
			.radius(3.0)
			.color(WHITE);
	}

    draw.to_frame(app, &frame).unwrap();
	draw2.to_frame(app, &frame).unwrap();
}

fn replot_if_too_close(star: &mut Star, others: &Vec<Star>) {
	let mut too_close = false;

	for other in others {
		if star.position.distance(other.position) < 30.0 {
			too_close = true;
			break;
		}
	}

	if too_close {
		star.plot(random_range(0.0,TAU), random_range(44.0, 3000.0));
		replot_if_too_close(star, &others);
	}
}
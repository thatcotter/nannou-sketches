use nannou::geom::Range;
use nannou::noise::{NoiseFn, Perlin};
use nannou::prelude::*;
use nannou::ui::prelude::*;
use std::convert::From;

struct Model {
    debug: bool,
    ui: Ui,
    ids: Ids,
    num_petals: i32,
    num_layers: i32,
    layer_distance: f32,
    magnitude: f32,
    start: f32,
    layer_rotation_offset: f32,
    alpha: f32,
}

struct Ids {
    num_petals: widget::Id,
    num_layers: widget::Id,
    layer_distance: widget::Id,
    magnitude: widget::Id,
    start: widget::Id,
    layer_rotation_offset: widget::Id,
    alpha: widget::Id,
}

fn main() {
    nannou::app(model).update(update).run();
}

fn model(app: &App) -> Model {
    app.new_window()
        .key_pressed(key_pressed)
        .view(view)
        .build()
        .unwrap();

    let mut ui = app.new_ui().build().unwrap();

    let ids = Ids {
        num_petals: ui.generate_widget_id(),
        num_layers: ui.generate_widget_id(),
        layer_distance: ui.generate_widget_id(),
        magnitude: ui.generate_widget_id(),
        start: ui.generate_widget_id(),
        layer_rotation_offset: ui.generate_widget_id(),
        alpha: ui.generate_widget_id(),
    };

    let debug = false;
    let num_petals = 24;
    let num_layers = 100;
    let layer_distance = 50.0;
    let magnitude = 25.0;
    let start = 0.8;
    let layer_rotation_offset = -PI;
    let alpha = 1.0;

    Model {
        debug,
        ui,
        ids,
        num_petals,
        num_layers,
        layer_distance,
        magnitude,
        start,
        layer_rotation_offset,
        alpha,
    }
}

fn key_pressed(_app: &App, model: &mut Model, key: Key) {
    match key {
        Key::Tab => {
            model.debug = !model.debug;
        }
        _ => {}
    }
}

fn update(app: &App, model: &mut Model, update: Update) {
    // Calling `set_widgets` allows us to instantiate some widgets.
    let ui = &mut model.ui.set_widgets();

    fn slider(val: f32, min: f32, max: f32) -> widget::Slider<'static, f32> {
        widget::Slider::new(val, min, max)
            .w_h(200.0, 30.0)
            .label_font_size(15)
            .rgb(0.3, 0.3, 0.3)
            .label_rgb(1.0, 1.0, 1.0)
            .border(0.0)
    }

    for value in slider(model.num_petals as f32, 1.0, 36.0)
        .top_left_with_margin(20.0)
        .label("Petals")
        .set(model.ids.num_petals, ui)
    {
        model.num_petals = value as i32;
    }

    for value in slider(model.num_layers as f32, 1.0, 120.0)
        .down(10.0)
        .label("Layers")
        .set(model.ids.num_layers, ui)
    {
        model.num_layers = value as i32;
    }

    for value in slider(model.layer_distance, 5.0, 120.0)
        .down(10.0)
        .label("Layer Distance")
        .set(model.ids.layer_distance, ui)
    {
        model.layer_distance = value;
    }

    for value in slider(model.magnitude, 0.0, 100.0)
        .down(10.0)
        .label("Length")
        .set(model.ids.magnitude, ui)
    {
        model.magnitude = value;
    }

    for value in slider(model.start, 0.0, 1.0)
        .down(10.0)
        .label("Length")
        .set(model.ids.start, ui)
    {
        model.start = value;
    }

    for value in slider(model.layer_rotation_offset, -TAU, TAU)
        .down(10.0)
        .label("Rotation")
        .set(model.ids.layer_rotation_offset, ui)
    {
        model.layer_rotation_offset = value;
    }

    for value in slider(model.alpha, 0.0, 1.0)
        .down(10.0)
        .label("Rotation")
        .set(model.ids.alpha, ui)
    {
        model.alpha = value;
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();
    let t = app.time;
    let win = app.window_rect();
    let noise = Perlin::new();
    let scale = 0.7;

    draw.background().color(Rgb::new(0.75, 0.75, 0.75));

    hallway(
        &draw,
        &t,
        0,
        scale,
        pt2(t * t * win.right(), t * t * win.top()),
        pt2(0.0, 0.0),
    );

    draw.to_frame(app, &frame).unwrap();
    if model.debug {
        model.ui.draw_to_frame(app, &frame).unwrap();
    }
}

fn hallway(draw: &app::Draw, t: &f32, step: i32, scale: f32, size: Point2, center: Point2) {
    let _scale = match scale {
        s if s > 0.9999 => 0.9999,
        s if s < 0.0 => 0.0,
        _ => scale,
    };

    let _center = pt2(
        center.x + (t + step as f32 * 0.0001).cos() * 15.0 * scale,
        center.y + (t + step as f32 * 0.0001).sin() * 15.0 * scale,
    );

    let half_thickness = 2.0;

    let inside_points = vec![
        _center - size * _scale,
        pt2(center.x + size.x * _scale, center.y - size.y * _scale),
        center + size * _scale,
        pt2(center.x - size.x * _scale, center.y + size.y * _scale),
    ];

    let points = vec![
        center - size,
        pt2(center.x + size.x, center.y - size.y),
        center + size,
        pt2(center.x - size.x, center.y + size.y),
    ];

    let vertices = (0..6).map(|i| points[i % 4]).enumerate().map(|(i, p)| {
        let sp = p * 0.01;
        let r = (t + sp.x).sin() * 0.5 + 0.5 - step as f32 * 0.009125;
        let g = (t + sp.y).cos() * 0.5 + 0.5 - step as f32 * 0.009125;
        let b = (t * 2.0 + sp.x).sin() * 0.5 + 0.5 - step as f32 * 0.009125;
        let hue = nannou::color::Rgba::new(r, g, b, 1.0);
        (p, hue)
    });

    draw.polyline().colored_points(vertices);

    for i in 0..4 {
        let p = points[i] * 0.01;
        let r = (t + p.x).sin() * 0.5 + 0.5 - step as f32 * 0.009125;
        let g = (t + p.y).cos() * 0.5 + 0.5 - step as f32 * 0.009125;
        let b = (t * 2.0 + p.x).sin() * 0.5 + 0.5 - step as f32 * 0.009125;
        let hue = nannou::color::Rgba::new(r, g, b, 1.0);
        draw.line()
            .start(inside_points[i])
            .end(points[i])
            .color(hue);
    }

    if size.x * _scale > 1.0 {
        hallway(&draw, &t, step + 1, scale, size * scale, _center)
    }
}

fn xy_from_theta(theta: f32) -> Point2 {
    let x = theta.cos();
    let y = theta.sin();
    pt2(x, y)
}

fn draw_diamond(
    draw: &app::Draw,
    center: Point2,
    angle: f32,
    length: f32,
    start: f32,
    color: Rgba,
) {
    let head = xy_from_theta(angle) * length;
    let left = xy_from_theta(angle + 0.05) * length * 0.9;
    let right = xy_from_theta(angle - 0.05) * length * 0.9;

    let points = vec![
        center + left,
        center + head,
        center + right,
        center + head * start,
    ];

    draw.polygon().points(points).color(color);
}

use helpers::xy_from_theta;
use nannou::geom::Range;
use nannou::noise::{NoiseFn, Perlin};
use nannou::prelude::*;
use nannou::ui::prelude::*;

struct Model {
    debug: bool,
    ui: Ui,
    ids: Ids,
    num_petals: i32,
    num_layers: i32,
    layer_distance: f32,
    magnitude: f32,
    start: f32,
    layer_rotation_offset: f32,
    alpha: f32,
}

struct Ids {
    num_petals: widget::Id,
    num_layers: widget::Id,
    layer_distance: widget::Id,
    magnitude: widget::Id,
    start: widget::Id,
    layer_rotation_offset: widget::Id,
    alpha: widget::Id,
}

fn main() {
    nannou::app(model).update(update).run();
}

fn model(app: &App) -> Model {
    app.new_window()
        .key_pressed(key_pressed)
        .view(view)
        .build()
        .unwrap();

    let mut ui = app.new_ui().build().unwrap();

    let ids = Ids {
        num_petals: ui.generate_widget_id(),
        num_layers: ui.generate_widget_id(),
        layer_distance: ui.generate_widget_id(),
        magnitude: ui.generate_widget_id(),
        start: ui.generate_widget_id(),
        layer_rotation_offset: ui.generate_widget_id(),
        alpha: ui.generate_widget_id(),
    };

    let debug = true;
    let num_petals = 24;
    let num_layers = 100;
    let layer_distance = 50.0;
    let magnitude = 25.0;
    let start = 0.8;
    let layer_rotation_offset = -PI;
    let alpha = 1.0;

    Model {
        debug,
        ui,
        ids,
        num_petals,
        num_layers,
        layer_distance,
        magnitude,
        start,
        layer_rotation_offset,
        alpha,
    }
}

fn key_pressed(_app: &App, model: &mut Model, key: Key) {
    match key {
        Key::Tab => {
            model.debug = !model.debug;
        }
        _ => {}
    }
}

fn update(app: &App, model: &mut Model, update: Update) {
    // Calling `set_widgets` allows us to instantiate some widgets.
    let ui = &mut model.ui.set_widgets();

    fn slider(val: f32, min: f32, max: f32) -> widget::Slider<'static, f32> {
        widget::Slider::new(val, min, max)
            .w_h(200.0, 30.0)
            .label_font_size(15)
            .rgb(0.3, 0.3, 0.3)
            .label_rgb(1.0, 1.0, 1.0)
            .border(0.0)
    }

    for value in slider(model.num_petals as f32, 1.0, 36.0)
        .top_left_with_margin(20.0)
        .label("Petals")
        .set(model.ids.num_petals, ui)
    {
        model.num_petals = value as i32;
    }

    for value in slider(model.num_layers as f32, 1.0, 120.0)
        .down(10.0)
        .label("Layers")
        .set(model.ids.num_layers, ui)
    {
        model.num_layers = value as i32;
    }

    for value in slider(model.layer_distance, 5.0, 120.0)
        .down(10.0)
        .label("Layer Distance")
        .set(model.ids.layer_distance, ui)
    {
        model.layer_distance = value;
    }

    for value in slider(model.magnitude, 0.0, 100.0)
        .down(10.0)
        .label("Length")
        .set(model.ids.magnitude, ui)
    {
        model.magnitude = value;
    }

    for value in slider(model.start, 0.0, 1.0)
        .down(10.0)
        .label("Length")
        .set(model.ids.start, ui)
    {
        model.start = value;
    }

    for value in slider(model.layer_rotation_offset, -TAU, TAU)
        .down(10.0)
        .label("Rotation")
        .set(model.ids.layer_rotation_offset, ui)
    {
        model.layer_rotation_offset = value;
    }

    for value in slider(model.alpha, 0.0, 1.0)
        .down(10.0)
        .label("Rotation")
        .set(model.ids.alpha, ui)
    {
        model.alpha = value;
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();
    let t = app.time;
    let _win = app.window_rect();
    let noise = Perlin::new();

    draw.background().color(Rgb::new(0.75, 0.75, 0.75));

    for i in 0..model.num_layers {
        for j in 0..model.num_petals {
            let center = pt2(0.0, 0.0);
            let angle = TAU * j as f32 / model.num_petals as f32
                + model.layer_rotation_offset * i as f32 / model.num_layers as f32
                + noise.get([t as f64, i as f64]) as f32 * 0.1
                + (t * 0.05).sin() * TAU;

            let r = (t * 1.0 + angle + i as f32 + j as f32).sin() * 0.5 + 0.75;
            let g = (t * 1.1 + angle + i as f32 + j as f32 + TAU * 0.33).sin() * 0.5 + 0.75;
            let b = (t * 0.9 + angle + i as f32 + j as f32 + TAU * 0.66).sin() * 0.5 + 0.75;

            draw_diamond(
                &draw,
                center,
                angle - t,
                model.magnitude * i as f32,
                model.start,
                Rgba::new(r, g, b, model.alpha),
            );
        }
    }

    draw.to_frame(app, &frame).unwrap();

    if (model.debug) {
        model.ui.draw_to_frame(app, &frame).unwrap();
    }
}

fn draw_diamond(draw: &Draw, center: Point2, angle: f32, length: f32, start: f32, color: Rgba) {
    let head = xy_from_theta(angle) * length;
    let left = xy_from_theta(angle + 0.05) * length * 0.9;
    let right = xy_from_theta(angle - 0.05) * length * 0.9;

    let points = vec![
        center + left,
        center + head,
        center + right,
        center + head * start,
    ];

    draw.polygon().points(points).color(color);
}

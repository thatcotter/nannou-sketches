extern crate nannou;
use nannou::draw::properties::{SetColor, SetDimensions};
use nannou::{prelude::*};
use nannou::noise::{NoiseFn, Perlin, Seedable};
use nannou::rand::{random_range};
// use nannou_conrod::Positionable;
// use nannou_conrod as ui;
// use nannou_conrod::prelude::*;
// use nannou::ui::prelude::*;

struct Model {
	// ui: Ui,
	// ids: Ids,
	// show_ui: bool,
	noise: Perlin,
	noise_scale: f32,
	noise_strength: f32,
	strokes: Vec<Stroke>,
	palette: Vec<Rgb>,
	min_width: f32,
	max_width: f32,
	decay_chance: f32
}

fn main() {
    nannou::app(model).update(update).run();
}



fn model(app: &App) -> Model {
	app.new_window()
    	.view(view)
		.key_pressed(key_pressed)
        .build()
        .unwrap();
	// app.set_loop_mode(LoopMode::loop_once());

	// Create the UI for our window.
	// let mut ui = ui::builder(app).window(w_id).build().unwrap();
	// // Generate some ids for our widgets.
    // let ids = Ids::new(ui.widget_id_generator());

	let min_width = 2.0;
	let max_width = 20.0;
	let decay_chance = 0.05;
	let palette = vec![
		rgb(1.0,1.0,1.0),
		rgb(1.0,0.0,0.0),
		rgb(0.0,1.0,0.0),
		rgb(0.0,0.0,1.0)
	];

	let noise = Perlin::new().set_seed(random_range(0, 10000));
	let noise_scale = 0.0025;
	let noise_strength = 2.0;

	let mut strokes = vec![];
	for i in 0..100 {
		strokes.push(Stroke::new(
			i,
			app.window_rect(),
			min_width, max_width,
			palette.clone(),
			decay_chance
			)
		);
	};

    Model {
		// ui,
		// ids,
		// show_ui: true,
		noise,
		noise_scale,
		noise_strength,
		strokes,
		palette,
		min_width,
		max_width,
		decay_chance
	}
}

fn key_pressed(app: &App, model: &mut Model, key: Key) {
	// clear_strokes(model.strokes);
	match key {
		Key::Space => {
			// model.strokes.into_iter().for_each(|mut stroke| {
			// 	stroke.path = Vec::new();
			// });
			let win = app.window_rect();
			for stroke in &mut model.strokes {
				stroke.path = vec![Vec2::new(
					random_range(win.left(), win.right()),
					random_range(win.top(), win.bottom())
				)];
				stroke.active = true;
				model.noise = Perlin::new().set_seed(random_range(0, 10000));
			}
		}
		Key::Tab => {
			// toggle ui
			// model.show_ui = !model.show_ui;
		}
		_ => {}
	}
}

fn update(app: &App, model: &mut Model, _update: Update) {

	let others = model.strokes.to_vec();

	let mut all_inactive = true;
	for stroke in &mut model.strokes {
		if stroke.active {
			all_inactive = false;
			stroke.update(model.noise, model.noise_scale, model.noise_strength, &others);
		}
	}

	if all_inactive {
		for i in model.strokes.len()..model.strokes.len()+50 {
			model.strokes.push(Stroke::new(
				i as u8,
				app.window_rect(),
				model.min_width, model.max_width,
				model.palette.clone(),
				model.decay_chance
			));
		}
	}

}

fn view(app: &App, model: &Model, frame: Frame){
	frame.clear(rgb(0.05, 0.05, 0.05));

	let draw = app.draw();
	let win = app.window_rect();
	let t = app.time;

	for stroke in &model.strokes {
		stroke.draw(&draw);
	}

    draw.to_frame(app, &frame).unwrap();
}

#[derive(Clone)]
struct Stroke {
	path: Vec<Point2>,
	width: f32,
	color: Rgb,
	active: bool,
	decay_chance: f32,
	id: u8
}

impl Stroke {
	fn new(id: u8, window: Rect, min_width: f32, max_width: f32, palette: Vec<Rgb>, decay_chance: f32) -> Stroke {
		let pos = Vec2::new(
			random_range(window.left(), window.right()),
			random_range(window.top(), window.bottom())
		);

		Stroke{
			path: vec![pos],
			width: random_range(min_width, max_width),
			color: palette[random_range(0, palette.len())],
			active: true,
			decay_chance,
			id
		}
	}

	fn update(&mut self, noise: Perlin, noise_scale: f32, noise_strength: f32, others: &Vec<Stroke>) {
		let pos = self.path[self.path.len()-1];

		for other in others {
			if self.id == other.id {
				continue;
			}
			for other_pos in &other.path{
				// if pos == *other_pos { continue; } 
				if pos.distance(*other_pos) < self.width * 0.66 {
					self.active = false;
					return;
				}
			}
		}

		if self.path.len() > 5 && random_f32() < self.decay_chance {
			self.active = false;
			return;
		}

		self.path.push(pos + Point2::new(
			noise.get([(pos.x * noise_scale) as f64, (pos.y * noise_scale) as f64]) as f32 * noise_strength,
			noise.get([(pos.y * noise_scale) as f64, (pos.x * noise_scale) as f64]) as f32 * noise_strength
		));
	}

	fn draw(&self, draw: &Draw) {
		draw.polyline()
			.weight(self.width)
			.end_cap_square()
			.join_round()
			.points(self.path.clone())
			.color(self.color);
	}
}
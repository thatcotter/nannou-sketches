use draw_helpers::line_square;
use nannou::prelude::*;

struct Model {}

fn main() {
    nannou::app(model).update(update).simple_window(view).run();
}

fn model(app: &App) -> Model {
    Model {}
}

fn update(app: &App, model: &mut Model, update: Update) {}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();
    let t = app.time;
    let win = app.window_rect();

    draw.background().color(Rgb::new(0.02, 0.02, 0.02));

    line_square(
        &app,
        &draw,
        pt2(0.0, 0.0),
        32,
        240.0,
        (t * 0.5).sin() * PI * 0.125,
    );

    draw.to_frame(app, &frame).unwrap();
}

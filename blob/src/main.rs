use nannou::noise::{NoiseFn, Perlin};
use nannou::prelude::*;

struct Model {}
type BlobPoints = Vec<(Point2, Rgba)>;

fn main() {
    nannou::app(model).update(update).simple_window(view).run();
}

fn model(_app: &App) -> Model {
    Model {}
}

fn update(_app: &App, _model: &mut Model, _update: Update) {}

fn view(app: &App, _model: &Model, frame: Frame) {
    let draw = app.draw();
    let t = app.time;

    draw.background().color(Rgb::new(0.83, 0.25, 0.062));

    let blobs = nested_blobs(64, pt2(0.0, 0.0), 15.0, 18.0, 0.0, 2.5, t);

    for blob in blobs {
        draw.polyline().weight(3.0).points_colored(blob);
    }

    draw.to_frame(app, &frame).unwrap();
}

fn nested_blobs(
    n_steps: i32,
    center: Point2,
    min_radius: f32,
    radius_step: f32,
    min_noise_amp: f32,
    amp_step: f32,
    time: f32,
) -> Vec<BlobPoints> {
    let blobs = (0..n_steps)
        .map(|i| {
            blob_points(
                center,
                min_radius + radius_step * i as f32,
                min_noise_amp + amp_step * i as f32,
                time,
            )
        })
        .collect();
    blobs
}

fn blob_points(center: Point2, radius: f32, noise_amp: f32, time: f32) -> BlobPoints {
    let noise = Perlin::new();
    let points = (0..=360)
        .map(|i| {
            let radian = deg_to_rad(i as f32);
            let xoff = (radian + radius / 114.0 + time * 1.6).sin() + 1.0;
            let yoff = (radian + radius / 114.0 + time * 1.2).sin() + 1.0;
            let x = radian.sin() * radius;
            let y = radian.cos() * radius;
            let n = noise_amp * noise.get([xoff as f64, yoff as f64]) as f32;
            (
                center + pt2(x, y) + pt2(n, n),
                rgba(1.0, 1.0, 1.0, 3.3 / (radius / 16.0)),
            )
        })
        .collect();
    points
}
